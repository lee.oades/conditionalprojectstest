﻿using System;

namespace Domain
{
    public interface IFooService
    {
        public void Run();
    }
}