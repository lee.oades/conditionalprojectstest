﻿using MediatR;

namespace Domain
{
    public class BarRequest : IRequest<BarResponse>
    {
        public string Name { get; set; }
    }
}