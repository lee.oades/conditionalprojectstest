﻿using System;
using System.Security.Authentication.ExtendedProtection;
using System.Threading.Tasks;
using Domain;
using MediatR;
using MediatR.Extensions.Microsoft.DependencyInjection.Libraries.Ext;
using Microsoft.Extensions.DependencyInjection;
using Plugins;

namespace ConditionalProjectsTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Setup registrations
            var container = new ServiceCollection()
                .AddClientGateways()
                .AddMediatRIncludingLibraries()
                .BuildServiceProvider();

            // Run a test service method
            container.GetService<IFooService>().Run();

            // Make a mediator request
            var mediator = container.GetService<IMediator>();
            var response = await mediator.Send(new BarRequest() {Name = "Lee"});
            Console.WriteLine(response.Message);
        }
    }


}