﻿Conditional Projects Test
==========================
##Objective
To create an app that can specify which "plugin" project to use.

 ###Plugins
 
Each gateway project has an extension method called *AddClientGateways* within the *Plugins* namespace. 
The main app is calling this. Obviously, it will run the one in the project that is referenced.

Note: Use Scrutor instead?

###Test App
There is a Property in ConditionalProjectsTest.csproj called *Client*.
It is being set to the name of the default client e.g. ClientA or ClientB.

    <PropertyGroup>
        <!-- This is the default Client e.g. the one Visual Studio will build -->
        <Client>ClientA</Client>
    </PropertyGroup>

Then through naming conventions, the corresponding gateway package is referenced:
 
     <PackageReference Include="ECommonSense.Integration.$(Client)" Version="1.0.0" />
 
 ###How to Build
 - Set a local directory as a nuget source.
 - Build the Domain package to this local directory
    
        dotnet pack .\Domain.sln -o .\LocalNugetSource\
    
 - Build the 2 gateway packages to this local directory.
 
        dotnet pack .\ClientA.sln -o .\LocalNugetSource\
        dotnet pack .\ClientB.sln -o .\LocalNugetSource\
        
 - Build ConditionalProjectsTest.sln using the appropriate property value
 
        dotnet build .\ConditionalProjectsTest.sln /p:Client=ClientA -o output/ClientA
        dotnet build .\ConditionalProjectsTest.sln /p:Client=ClientB -o output/ClientB
 
 
 
 
 