﻿using ClientAGateways.Services;
using Domain;
using MediatR.Extensions.Microsoft.DependencyInjection.Libraries.Ext;
using Microsoft.Extensions.DependencyInjection;

namespace Plugins
{
    public static class ContainerEx
    {
        public static IServiceCollection AddClientGateways(this IServiceCollection sc)
        {
            return sc
                .AddTransient<IFooService, FooService>()
                .AddMeditatRLibrary(typeof(ContainerEx).Assembly);
        }
    }
}