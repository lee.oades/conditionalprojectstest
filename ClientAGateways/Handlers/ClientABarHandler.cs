﻿using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;

namespace ClientAGateways.Handlers
{
    public class ClientABarHandler :IRequestHandler<BarRequest, BarResponse>
    {
        public Task<BarResponse> Handle(BarRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(new BarResponse
            {
                Message = $@"Hello {request.Name}, from ClientABarHandler in Client A!"
            });
        }
    }
}