﻿using System;
using Domain;

namespace ClientAGateways.Services
{
    public class FooService : IFooService
    {
        public void Run()
        {
            Console.WriteLine("Hello from FooService in Client A");
        }
    }
}