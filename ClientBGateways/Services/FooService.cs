﻿using System;
using Domain;

namespace ClientBGateways.Services
{
    public class FooService : IFooService
    {
        public void Run()
        {
            Console.WriteLine("Hola from the FooService in Client B");
        }
    }
}