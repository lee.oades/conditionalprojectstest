﻿using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;

namespace ClientBGateways.Handlers
{
    public class ClientBBarHandler :IRequestHandler<BarRequest, BarResponse>
    {
        public Task<BarResponse> Handle(BarRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(new BarResponse
            {
                Message = $@"Wassup {request.Name}! This is ClientBBarHandler from Client B!"
            });
        }
    }
}